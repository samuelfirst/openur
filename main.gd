extends Node

# ================================== ATTRIBUTES ================================
# An array representing the state of the board
#
#                00 01 02
#                03 04 05
#                06 07 08
#                09 10 11
#                   12
#                   13
#                14 15 16
#                17 18 19
#
# The slots can contain either "e", meaning the position is empty,
# "w", meaning that the position is filled by a white tile, or
# "b", meaning that the position is filled by a black tile.
var BOARD := ["e", "e", "e", "e",
	          "e", "e", "e", "e",
	          "e", "e", "e", "e",
	          "e", "e", "e", "e",
	          "e", "e", "e", "e"]

# An array containing the locations of the lotus tiles
var LOTUS_TILES := [0, 2, 10, 14, 16]

# Data Structure for Holding Player Data
# Consists of an array with a hash table for each player holding that player's
# data.  This is a useful construct for the following  reason: the array can be
# reversed so that PLAYERS[0] is always the player whose turn it is and
# PLAYERS[1] is always their opponent -- this eliminates a lot of redundant
# branching code.  Arrays and hash tables are also some of the only constructs
# in GDScript that are passed by reference, so it should be possible to define
# more descriptive variables (ex. player, other_player) pointing at the members
# of the array and use them to modify the values in the global scope.
#
# The data is stored as follows:
#  +-----------------+-----------+---------------------------------------------+
#  |      ENTRY      |    TYPE   |                    PURPOSE                  |
#  +=================+===========+=============================================+
#  | color           | String    | The player color (white or black)           |
#  | tile            | String    | The char to use with BOARD for the player   |
#  | score           | int       | The player's score                          |
#  | reserve         | int       | The number of reserve pieces the player has |
#  | path            | Array:int | The path the player's pieces follow         |
#  | roll message    | String    | The message for when the player rolls       |
#  | winning message | String    | The message for when the player wins        |
#  +-----------------+-----------+---------------------------------------------+
var PLAYERS = [{"color": "white",
	            "tile": "w",
	            "score": 0,
	            "reserve": 7,
	            "path": [9, 6, 3, 0, 1, 4, 7, 10, 12, 13, 15, 18, 17, 14],
	            "roll message": "White's Turn",
	            "winning message": "White Wins!"},
	            	
	           {"color": "black",
				"tile": "b",
				"score": 0,
				"reserve": 7,
				"path": [11, 8, 5, 2, 1, 4, 7, 10, 12, 13, 15, 18, 19, 16],
				"roll message": "Black's Turn",
				"winning message": "Black Wins!"}]
# ==============================================================================

# =============================== DEBUGGING FUNCTIONS ==========================
func print_scores():
	var scores = {"white": [0, 0], "black": [0, 0]}
	for player in PLAYERS:
		scores[player["color"]] = [player["score"], player["reserve"]]
	print("WS/WR: |%s/%s|" % scores["white"])
	print("BS/BR: |%s/%s|" % scores["black"])

# Print the board to console for debug purposes
func print_board():
	print("============")
	print_scores()
	print("[%s][%s][%s]" % BOARD.slice(0, 3))
	print("[%s][%s][%s]" % BOARD.slice(3, 6))
	print("[%s][%s][%s]" % BOARD.slice(6, 9))
	print("[%s][%s][%s]" % BOARD.slice(9, 12))
	print("   [%s]     " % BOARD[12])
	print("   [%s]     " % BOARD[13])
	print("[%s][%s][%s]" % BOARD.slice(14, 17))
	print("[%s][%s][%s]" % BOARD.slice(17, 20))
	print("============")
# ==============================================================================
	

# Function to map a tile from the BOARD array to an offset usable by one of
# the PathFollow2D nodes.  The path to use (WHITE_PATH or BLACK_PATH) and the
# tile (an index of BOARD) to return the offset for must be passed.
func map_tile_to_path_pos(path, tile):
	var offset: int = path.find(tile)
	return offset * 64

# Function to map a path position from a PathFollow2D node to a tile from the
# BOARD array.  The path to use (WHITE_PATH or BLACK_PATH) and the position
# from the PathFollow2D node (PathFollow2D.progress) must be passed as arguments.
func map_path_pos_to_tile(path, path_pos):
	var index: int = path_pos / 64
	return path[index]

# Create a piece for the specified player at the specified position
func create_piece(pos: int):
	var player = PLAYERS[0]
	BOARD[pos] = player["tile"]
	await $Board.add_piece(player["color"], map_tile_to_path_pos(player["path"],
		                   pos))
	player["reserve"] -= 1
	$Board.set_reserve(player["color"], player["reserve"])

# Move the piece in 'initial' to 'final'
func move_piece(initial: int, final: int):
	var player = PLAYERS[0]
	var other_player = PLAYERS[1]

	BOARD[initial] = 'e'
	if final != player["path"][-1]:
		await $Board.move_piece(player["color"],
			                    map_tile_to_path_pos(player["path"], initial),
			                    map_tile_to_path_pos(player["path"], final))
		if BOARD[final] != 'e':
			other_player["reserve"] += 1
			$Board.set_reserve(other_player["color"], other_player["reserve"])
			$Board.remove_piece(other_player["color"],
				                map_tile_to_path_pos(other_player["path"],
				                                     final))
		BOARD[final] = player["tile"]
	else:
		await $Board.move_piece(player["color"],
			                    map_tile_to_path_pos(player["path"], initial),
			                    map_tile_to_path_pos(player["path"], final))
		$Board.remove_piece(player["color"],
			                map_tile_to_path_pos(player["path"], final))
		player["score"] += 1
		$Board.set_score(player["color"], player["score"])

# Determine whether a given move is legal and return the correct BOARD index
# player (String): either "white" or "black"
# tile (int): the index from BOARD of the piece to move
# roll (int): the number of spaces to move, determined by $Board.roll()
#
# -1 can be passed as the tile for a reserve piece.
# If the move is illegal, a value of -1 will be returned.
# Illegal moves are moves that would move the player's piece to a tile they
# already have a piece on or the central lotus tile if it is already occupied.
func get_move(tile: int, roll: int):
	var player = PLAYERS[0]
	var initial: int
	var final: int

	if tile == -1:
		initial = -1
		if player["reserve"] == 0:
			return -1
	else:
		initial = player["path"].find(tile)

	if initial + roll > player["path"].size() - 1:
		return -1
	else:
		final = player["path"][initial + roll]

	var occupied_by_player: bool = (BOARD[final] == player["tile"])
	var is_center_lotus_and_occupied: bool = (final == 10 and BOARD[10] != 'e')
		
	if occupied_by_player or is_center_lotus_and_occupied:
		#(occupied_by_player and BOARD[tile] != 'e') or \
		#(LOTUS_TILES.has(final) and BOARD[final] != 'e'):
		return -1
	else:
		return final

# Determine whether a player has any legal moves; return true or false
func valid_moves(roll):
	var player = PLAYERS[0]
	
	if roll == 0:
		return false
	for i in range(BOARD.size()):
		if BOARD[i] == player["tile"]:
			if get_move(i, roll) != -1:
				return true
	if BOARD[player["path"][roll - 1]] == 'e' and player["reserve"] != 0:
		return true
	return false

# Reset everything for a new round of play
func reset_game():
	if PLAYERS[0]["color"] != "white":
		PLAYERS.reverse()
	for player in PLAYERS:
		player["score"] = 0
		player["reserve"] = 7
	for i in range(BOARD.size()):
		BOARD[i] = 'e'
	
# Set up the board for a game to be played
func set_up_board():
	$Main_Menu.hide()
	reset_game()
	$Board.reset()
	$Board.show()

func set_up_menu():
	$Board.hide()
	$Main_Menu.show()

# Display the roll message for the current player and return the roll result
func get_roll():
	$Overlay.display(PLAYERS[0]["roll message"], "Roll")
	await $Overlay.overlay_signal
	return await $Board.roll_dice()

# Get the player's move
func get_player_move(roll: int):
	var player = PLAYERS[0]
	var other_player = PLAYERS[1]
	var player_sel: Array
	var piece_pos: int
	var mapped_piece_pos: int
	var move: int
	var move_pos: int
	var mapped_move_pos: int
	var not_player_color: bool
	var not_occupied: bool
	var not_neutral: bool
	
	while true:
		player_sel = await $Board.player_selection
		if player_sel[0] != player["color"]:
			await $Board.selections_done # <--- This keeps the order of selecting
			$Board.clear_indications()   #      and clearing correct.
			continue
		
		piece_pos = player_sel[1]
		if piece_pos == -64: # Hacky; probably needs to be refactored
			mapped_piece_pos = -1
		else:
			mapped_piece_pos = map_path_pos_to_tile(player["path"], piece_pos)
				
		player_sel = await $Board.player_selection
		move_pos = player_sel[1]
		mapped_move_pos = map_path_pos_to_tile(player["path"], move_pos)
			
		not_player_color = (player_sel[0] != player["color"])
		not_occupied = (BOARD[mapped_move_pos] != other_player["tile"])
		not_neutral = (player_sel[0] != "neutral")
			
		if (not_player_color and not_occupied) and not_neutral:
			continue
			
		move = get_move(mapped_piece_pos, roll)
			
		if move != -1 and move == mapped_move_pos:
			if mapped_piece_pos == -1:
				await create_piece(move)
			else:
				await move_piece(mapped_piece_pos, mapped_move_pos)
			break

	return mapped_move_pos

# Get the move for the computer player
func get_ai_move(roll):
	var player = PLAYERS[0]
	var other_player = PLAYERS[1]
	var ai_pieces = []
	var moves := []

	
	# Get all the pieces on the board
	for i in range(BOARD.size()):
		if BOARD[i] == player["tile"]:
			ai_pieces.push_back(i)
			
	# Consolidate all the possible moves into the moves array
	# The moves are stored as an array with the following data:
	#   [move tile (int), move weight (int), initial tile (int)]
	if get_move(-1, roll) != -1:
		moves.push_back([get_move(-1, roll), 0, -1])
		
	for piece in ai_pieces:
		if get_move(piece, roll != -1):
			moves.push_back([get_move(piece, roll), 0, piece])

	# Run through the moves array and weight the moves
	for move in moves:
		if move[0] == player["path"][-1]: # Move piece to end of board
			move[1] += 19
		if move[0] == 10: # Middle tile
			move[1] += 18
		if move[0] == 2: # Lotus tile
			move[1] += 17
		if BOARD[move[0]] == other_player["tile"]: # Capture
			move[1] += 16
		if [11, 8, 5].has(move[0]): # Marshal pieces in home row
			move[1] += 15
		move[1] += player["path"].find(move[0]) # Weight by how far in path

	# Sort the array by weight
	moves.sort_custom(func(a, b): return (a[1] > b[1]))

	# Make the move
	if moves[0][2] == -1:
		await create_piece(moves[0][0])
	else:
		await move_piece(moves[0][2], moves[0][0])

	# Return the move
	await get_tree().create_timer(0.5).timeout
	return moves[0][0]

# Check to see if the current player has any valid moves
func check_valid_moves(roll):
	if not valid_moves(roll):
		$Overlay.display("No Valid Moves", "Continue")
		await $Overlay.overlay_signal
		PLAYERS.reverse()
		return false
	return true

# Place animated fireworks around the screen at random locations
func fireworks():
	var bounds: Vector2 = get_viewport().get_visible_rect().size
	var textures := ["res://assets/fireworks/fireworks_blue.png",
		             "res://assets/fireworks/fireworks_green.png",
		             "res://assets/fireworks/fireworks_pink.png",
		             "res://assets/fireworks/fireworks_purple.png",
		             "res://assets/fireworks/fireworks_red.png",
		             "res://assets/fireworks/fireworks_white.png",
		             "res://assets/fireworks/fireworks_yellow.png"]
	var point: Vector2
	var firework: Sprite2D

	# Lambda for the individual firework animations; allow us to async them
	var boom = func(x: Sprite2D):
		for i in range(7):
			x.set_frame(i)
			await get_tree().create_timer(randf_range(0.06, 0.10)).timeout
		x.queue_free()

	for i in range(30):
		point = Vector2(randi_range(0, bounds.x), randi_range(0, bounds.y))
		firework = Sprite2D.new()
		firework.set_texture(load(textures.pick_random()))
		firework.set_vframes(7)
		firework.set_scale(Vector2(6, 6))
		firework.set_position(point)
		add_child(firework)
		boom.call(firework)
		await get_tree().create_timer(randf_range(0.0, 0.3)).timeout

# Check to see if the current player has won
func check_for_win():
	var player = PLAYERS[0]
	if player["score"] == 7:
		await fireworks()
		$Overlay.display(player["winning message"], "Continue")
		await $Overlay.overlay_signal
		return true
	return false

# Game loop for a one player game
func game_loop_1p():
	var player: Dictionary
	var is_human_move: bool
	var roll: int
	var move: int

	# Loop forever; we break on winning conditions within the loop
	while true:
		player = PLAYERS[0]
		is_human_move = (player["color"] == "white")

		# Get roll and move for the human player
		if is_human_move:
			roll = await get_roll()
			
			if not await check_valid_moves(roll):
				continue

			move = await get_player_move(roll)
			
		# Get the roll and move for the ai player
		else:
			roll = await $Board.roll_dice()
			
			if not valid_moves(roll):
				PLAYERS.reverse()
				continue

			move = await get_ai_move(roll)

		# Check for winning conditions
		if await check_for_win():
			break

		# Swap the player and repeat (unless the last move landed on a lotus)
		if not LOTUS_TILES.has(move):
			PLAYERS.reverse()

# Game loop for a two player game
func game_loop_2p():
	var player: Dictionary
	var roll: int
	var move: int

	# Loop forever; we break on winning conditions within the loop
	while true:
		player = PLAYERS[0]
		
		# Prompt the player to roll and roll dice
		roll = await get_roll()

		# Check to see if any valid moves exist
		if not await check_valid_moves(roll):
			continue

		# Get the player's move
		move = await get_player_move(roll)

		# Check for winning conditions
		if await check_for_win():
			break

		# Swap the player and repeat (unless the last move landed on a lotus)
		if not LOTUS_TILES.has(move):
			PLAYERS.reverse()

# Function to move the tile specified forward two spaces (for use with
# `game_loop_htp()`).
# -1 can be passed for the reserve tiles.
func htp_forward_two(start):
	await $Board.fake_roll()
	await get_tree().create_timer(0.4).timeout
	if start == -1:
		await create_piece(PLAYERS[0]["path"][1])
	else:
		await move_piece(PLAYERS[0]["path"][start],
			             PLAYERS[0]["path"][start + 2])
		
# Game loop (I mean, sort of) for the instructions on how to play
func game_loop_htp():
	var sleep_time: float = 0.4
	
	# The Basics
	$Instructions.show_instruction()
	await $Instructions.continue_selected
	for n in range(2):
		await create_piece(PLAYERS[0]["path"][0])
		for i in range(1, PLAYERS[0]["path"].size()):
			await move_piece(PLAYERS[0]["path"][i - 1], PLAYERS[0]["path"][i])
			await get_tree().create_timer(sleep_time).timeout
		PLAYERS.reverse()

	reset_game()
	$Board.reset()

	# Movement
	$Instructions.show_instruction()
	await $Instructions.continue_selected
	await htp_forward_two(-1)
	PLAYERS.reverse()
	await htp_forward_two(-1)
	PLAYERS.reverse()
	await get_tree().create_timer(sleep_time).timeout

	# Capturing
	$Instructions.show_instruction()
	await $Instructions.continue_selected
	await htp_forward_two(1)
	await htp_forward_two(3)
	PLAYERS.reverse()
	await htp_forward_two(1)
	await htp_forward_two(3)
	await get_tree().create_timer(sleep_time).timeout

	# Lotus Tiles
	$Instructions.show_instruction()
	await $Instructions.continue_selected
	await htp_forward_two(5)
	await htp_forward_two(7)
	await get_tree().create_timer(sleep_time).timeout

# Called when the signal start_1p is emitted by the menu scene
func _on_start_1p():
	set_up_board()
	await game_loop_1p()
	set_up_menu()

# Called when the signal start_2p is emitted by the menu scene
func _on_start_2p():
	set_up_board()
	await game_loop_2p()
	set_up_menu()

# Called when the signal show_instructions is emitted by the menu scene
func _on_show_instructions():
	set_up_board()
	await game_loop_htp()
	set_up_menu()
