extends Control

# Signals
signal continue_selected

# Array containing the title text for each instruction
var TITLES := ["The Basics",
	           "Movement",
	           "Capturing",
	           "Lotus Tiles"]

# Array containing the body text for each instruction
var INSTRUCTIONS  := ["Each player starts with 7 pieces in reserve.  The goal of\
 the game is to move all of your pieces around the board to the final tile\
 before your opponent.  Each player's pieces follow a set path around the\
 board.",
	                  "Movement is determined by a dice roll.  Each die has four\
 sides, two of which are marked with red tips.  For each die that lands with a\
 red tip facing up, you may move a piece forward one space.  If you roll 0 or no\
 moves are possible, your turn is skipped.",
	                  "You may move one of your pieces over your opponents piece\
 to capture that piece.  Capturing a piece returns it to the opponent's\
 reserve.",
	                  "If a piece lands on a lotus tile, the player who moved it\
 gets an extra turn.  Additionally, a piece occupying the central lotus tile may\
 not be captured."]

# Rotate through the bodys/titles by popping them off the front of their arrays,
# setting the text box text to them, and pushing them to the back of the arrays.
func show_instruction():
	var title: String = TITLES.pop_front()
	var body: String = INSTRUCTIONS.pop_front()

	$Box/Title.set_text(title)
	$Box/Body.set_text(body)
	self.show()

	TITLES.push_back(title)
	INSTRUCTIONS.push_back(body)

# When the continue button is pressed, emit the continue_selected signal
func _on_continue():
	self.hide()
	continue_selected.emit()
