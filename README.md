# OpenUr

![Screenshot of OpenUr Board](/images/screenshot.png)

The Royal Game of Ur originated in Mesopotamia nearly 5000 years ago circa 2600
BCE.  The precursor to the family of tables games like backgammon, the Royal
Game enjoyed popularity across social class divides.  Eventually the game faded
from the public consciousness and the rules would have been lost to time, if not
for the rules tablets uncovered Sir Leonard Woolley's excavation of the Royal 
Cemetary of Ur.  The game falls into the broader category of racing games, in 
which players compete to race pieces around a board and combines elements of
strategy and luck.[\[1\]](https://en.wikipedia.org/wiki/Royal_Game_of_Ur)

OpenUr aims to bring the Royal Game of Ur from the bronze age into the silicon 
age.

----

## Where to Play

The game can be found on the following platforms:

|                                                                   | Linux | Windows | Android | Web |
|------------------------------------------------------------------:|:-----:|:-------:|:-------:|:---:|
|                          [Itch.io](https://sfirst.itch.io/openur) | x     | x       | x       | x   |
|                                   The F-Droid Store (placeholder) |       |         | x       |     |
| [Releases Page](https://gitlab.com/samuelfirst/openur/-/releases) | x     | x       | x       |     |

## License

* The Textual files (*.gd, *.tscn, *.tres, *.godot) are licensed under the
  GPLv3.  See [Licenses/GPL](licenses/GPL) for more information.
* The image assets (*.png, *.svg, *.pxo) are licensed under Creative Commons
  Share-alike Non Commercial (CC-BY-SA-NC).  See 
  [Licenses/CC-BY-SA-NC](licenses/CC-BY-SA-NC) for more information.
* The Junicode font (assets/junicode/*.ttf) is licensed under the Open Font
  License (OFL).  See [Licenses/OFL`](licenses/OFL) for more information.
