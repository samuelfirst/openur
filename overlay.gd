extends Control

signal overlay_signal

func _on_button():
	self.hide()
	overlay_signal.emit()

# Shows the roll dialog box and sets the player name to the passed string
# (expected to be "White" or "Black")
func display(label_text, button_text):
	$Box/Overlay_Label.set_text(label_text)
	$Box/Overlay_Button.set_text(button_text)
	self.show()

