extends Node2D

# SIGNALS
signal player_selection(return_val: Array)
signal selections_done

# Reset the board to the starting state
func reset():
	$White_Reserve_Label.set_text("7")
	$Black_Reserve_Label.set_text("7")
	$White_Score_Label.set_text("0")
	$Black_Score_Label.set_text("0")
	$Roll_Label.set_text("-")

	for piece in $White_Path2D.get_children():
		piece.queue_free()

	for piece in $Black_Path2D.get_children():
		piece.queue_free()

	for die in [$Die_1, $Die_2, $Die_3, $Die_4]:
		die.set_texture(load("res://assets/dice/Die_0001.png"))

# Set the score label for the specified player (str) to the specified score (int)
func set_score(player, score):
	if player == "white":
		$White_Score_Label.set_text(str(score))
	elif player == "black":
		$Black_Score_Label.set_text(str(score))

# Set the reserve label for the specified player (str) to the specified score (int)
func set_reserve(player, score):
	if player == "white":
		$White_Reserve_Label.set_text(str(score))
	elif player == "black":
		$Black_Reserve_Label.set_text(str(score))

# Roll each die and return the total of the results
func roll_dice():
	$Roll_Label.set_text("-")
	var score: int = 0
	var sleep_time: float = 0.05
	var states = [["res://assets/dice/Die_0001.png", 1],
	              ["res://assets/dice/Die_0002.png", 1],
	              ["res://assets/dice/Die_0003.png", 0],
	              ["res://assets/dice/Die_0004.png", 0]]
	var dice = [$Die_1, $Die_2, $Die_3, $Die_4]

	for i in range(10):
		for die in dice:
			var state = states.pick_random()
			var texture: Texture2D = load(state[0])
			die.set_texture(texture)
			if i == 9:
				score += state[1]
			await get_tree().create_timer(sleep_time).timeout

	$Roll_Label.set_text(str(score))
	return score

# Fake rolling the dice; will always show a roll of two
# For use with the instructions on how to play.
func fake_roll():
	await roll_dice()
	await get_tree().create_timer(0.05).timeout
	$Die_1.set_texture(load("res://assets/dice/Die_0001.png"))
	$Die_2.set_texture(load("res://assets/dice/Die_0002.png"))
	$Die_3.set_texture(load("res://assets/dice/Die_0003.png"))
	$Die_4.set_texture(load("res://assets/dice/Die_0004.png"))
	$Roll_Label.set_text(str(2))

# Animates the move for the passed piece by moving it along its parent path2d
# object 10 px at a time with a delay between
func animate_move(piece, init, final):
	piece.set_z_index(1)
	for i in range(init, final, 10):
		piece.set_progress(i)
		await get_tree().create_timer(0.01).timeout
	piece.set_z_index(0)

# Checks to make sure no pieces are indicated and returns true or false
# This is for use with `indicate_piece()`, to make sure that we only highlight
# pieces when they are actually selected.
func nothing_indicated():
	for reserve in [$WRP, $BRP]:
		if reserve.get_modulate() == Color("940b0b"):
			return false
		
	for path in [$White_Path2D, $Black_Path2D]:
		for follower in path.get_children():
			if follower.get_child(0).get_modulate() == Color("940b0b"):
				return false
	return true

# Clear out all indicated pieces
func clear_indications():
	for reserve in [$WRP, $BRP]:
		reserve.set_modulate(Color(1, 1, 1, 1))
		
	for path in [$White_Path2D, $Black_Path2D]:
		for follower in path.get_children():
			follower.get_child(0).set_modulate(Color(1, 1, 1, 1))
	
# Highlights a piece with "940b0b" when the piece is selected, then unhighlights
# it on the next selection.  There's a bit of signal wrestling here; the order in
# which the signals are connected is vital for this to work: indicate_piece *has*
# to be connected before connecting signal_selected.
func indicate_piece(event, texturerect):
	if event is InputEventMouseButton and event.is_pressed() and\
		        nothing_indicated():
		await player_selection # Signal emitted by pressing our piece
		texturerect.set_modulate(Color("940b0b"))
		selections_done.emit()
		await player_selection # Signal emitted by pressing another piece
		texturerect.set_modulate(Color(1, 1, 1, 1))
	
# Add a piece for the passed player at the position specified in progress.
# The added piece consists of the following nodes:
#
#    \ PathFollow2D		   || For moving the piece along the Path2D node
#     \- TextureRect       || For displaying a clickable sprite
#
# Note that progress is the progress property of the piece's PathFollow2D
# component, and should be calculated with the `map_tile_to_path_pos` function
# in 'main.gd'.
func add_piece(player, progress):
	var follower := PathFollow2D.new()
	var texturerect := TextureRect.new()
	var res = {"white": "res://assets/Piece_White.png",
		       "black": "res://assets/Piece_Black.png"}
	var paths = {"white": $White_Path2D,
		         "black": $Black_Path2D}
	var texture: Texture2D = load(res[player])

	texturerect.set_texture(texture)
	texturerect.position = Vector2(-32, -32)
	texturerect.gui_input.connect(indicate_piece.bind(texturerect))
	texturerect.gui_input.connect(signal_selected.bind(player, progress))
	follower.add_child(texturerect)
	
	follower.set_loop(false)
	paths[player].add_child(follower)

	await animate_move(follower, 0, progress)
	follower.set_progress(progress)

# Remove the specified piece from the board by queue_free()ing it
func remove_piece(player, progress):
	var paths = {"white": $White_Path2D,
		         "black": $Black_Path2D}
	for piece in paths[player].get_children():
		# The roundi fixes a weird bug where setting the progress for black
		# to 832 (the final square) would actually set it to 831.999877929688.
		if roundi(piece.progress) == progress:
			piece.queue_free()

# Move the specified piece
# Also, disconnect the selection signal and reconnect it with the new position
func move_piece(player, init_pos, final_pos):
	var paths = {"white": $White_Path2D,
		         "black": $Black_Path2D}
	for piece in paths[player].get_children():
		if piece.get_progress() == init_pos:
			await animate_move(piece, init_pos, final_pos)
			piece.set_progress(final_pos)
			var texturerect = piece.get_child(0)
			texturerect.gui_input.disconnect(signal_selected)
			texturerect.gui_input.disconnect(indicate_piece)
			texturerect.gui_input.connect(indicate_piece.bind(texturerect))
			texturerect.gui_input.connect(signal_selected.bind(player,
				                                               final_pos))
			break

# Pass a signal with the selected path pos up when a piece or tile is selected
func signal_selected(event, color, path_pos: int):
	if event is InputEventMouseButton and event.is_pressed():
		player_selection.emit([color, path_pos])
	
# Called when the node enters the scene tree for the first time.
func _ready():
	for tile in self.get_tree().get_nodes_in_group("board_tiles"):
		tile.gui_input.connect(signal_selected.bind(tile.get_meta("player"),
			                                        tile.get_meta("index")))

	$WRP/White_Reserve_Piece.gui_input.connect(indicate_piece.bind($WRP))
	$WRP/White_Reserve_Piece.gui_input.connect(signal_selected.bind("white",-64))
	$BRP/Black_Reserve_Piece.gui_input.connect(indicate_piece.bind($BRP))
	$BRP/Black_Reserve_Piece.gui_input.connect(signal_selected.bind("black",-64))
