extends Control

# SIGNALS
signal start_1p
signal start_2p
signal show_instructions



func _on_1p_button_pressed():
	self.hide()
	start_1p.emit()

func _on_2p_button_pressed():
	self.hide()
	start_2p.emit()

func _on_htp_button_pressed():
	self.hide()
	show_instructions.emit()
